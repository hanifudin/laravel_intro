<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('form');
    }

    public function kirim(Request $request)
    {
        $name1 = $request->namadepan;
        $name2 = $request->namabelakang;

        return view('welcome',compact('name1','name2'));

        
    }

    public function welcome(){
        return view('welcome');
    }
}
